package com.revolve44.battletank.models

import com.revolve44.battletank.enums.Material

data class Element (
    val viewId: Int,
    val material: Material,
    val coordinate: Coordinate
)