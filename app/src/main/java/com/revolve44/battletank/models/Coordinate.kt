package com.revolve44.battletank.models

data class Coordinate(
    val top: Int,
    val left: Int
)