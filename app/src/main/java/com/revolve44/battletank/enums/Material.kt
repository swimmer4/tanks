package com.revolve44.battletank.enums

enum class Material (val tankCanGoThrough: Boolean) {
    EMPTY(true),
    BRICK(false),
    CONCRETE(false),
    GRASS(true),

}
