package com.revolve44.battletank.enums

enum class Direction(val rotation: Float) {
    UP(0f),
    RIGHT(90f),
    LEFT(270f),
    BOTTOM(180f),
}