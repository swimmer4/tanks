package com.revolve44.battletank.utils

import android.view.View
import com.revolve44.battletank.HORIZONTAL_MAX_SIZE
import com.revolve44.battletank.VERTICAL_MAX_SIZE
import com.revolve44.battletank.models.Coordinate


fun View.checkViewCanMoveThroughBorder(coordinate: Coordinate): Boolean {
    if (coordinate.top >= 0
        && coordinate.top + this.height <= HORIZONTAL_MAX_SIZE
        && coordinate.left >= 0
        && coordinate.left + this.width <= VERTICAL_MAX_SIZE
    ) {
        return true
    }
    return false
}